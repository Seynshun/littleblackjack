class Player {
  constructor(money, bet) {
    this.gameValue = 0;
    this.money = money;
    this.bet = bet;
  }

  setGameValue(gameValue) {
    this.gameValue = parseInt(gameValue);
  }
  updateGameValue(gameValue) {
    this.gameValue += parseInt(gameValue);
  }
  setMoney(money) {
    this.money = money;
  }

  updateMoney(bet) {
    console.log(this.money);
    console.log("bet" + bet);
    this.money += bet;
    console.log(this.money);
  }

  getGameValue() {
    return this.gameValue;
  }
  getMoney() {
    return this.money;
  }

  getBet() {
    return this.bet;
  }
}
