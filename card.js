const deck = [
  { "cards/AC.svg": 1 },
  { "cards/AD.svg": 1 },
  { "cards/AH.svg": 1 },
  { "cards/AS.svg": 1 },
  { "cards/2C.svg": 2 },
  { "cards/2D.svg": 2 },
  { "cards/2H.svg": 2 },
  { "cards/2S.svg": 2 },
  { "cards/3C.svg": 3 },
  { "cards/3D.svg": 3 },
  { "cards/3H.svg": 3 },
  { "cards/3S.svg": 3 },
  { "cards/4C.svg": 4 },
  { "cards/4D.svg": 4 },
  { "cards/4H.svg": 4 },
  { "cards/4S.svg": 4 },
  { "cards/5C.svg": 5 },
  { "cards/5D.svg": 5 },
  { "cards/5H.svg": 5 },
  { "cards/5S.svg": 5 },
  { "cards/6C.svg": 6 },
  { "cards/6D.svg": 6 },
  { "cards/6H.svg": 6 },
  { "cards/6S.svg": 6 },
  { "cards/7C.svg": 7 },
  { "cards/7D.svg": 7 },
  { "cards/7H.svg": 7 },
  { "cards/7S.svg": 7 },
  { "cards/8C.svg": 8 },
  { "cards/8D.svg": 8 },
  { "cards/8H.svg": 8 },
  { "cards/8S.svg": 8 },
  { "cards/9C.svg": 9 },
  { "cards/9D.svg": 9 },
  { "cards/9H.svg": 9 },
  { "cards/9S.svg": 9 },
  { "cards/10C.svg": 10 },
  { "cards/10D.svg": 10 },
  { "cards/10H.svg": 10 },
  { "cards/10S.svg": 10 },
  { "cards/JC.svg": 10 },
  { "cards/JD.svg": 10 },
  { "cards/JH.svg": 10 },
  { "cards/JS.svg": 10 },
  { "cards/QC.svg": 10 },
  { "cards/QD.svg": 10 },
  { "cards/QH.svg": 10 },
  { "cards/QS.svg": 10 },
  { "cards/KC.svg": 10 },
  { "cards/KD.svg": 10 },
  { "cards/KH.svg": 10 },
  { "cards/KS.svg": 10 },
];
class Card {
  constructor() {
    const card = this.randomCard();
    this.path = Object.keys(card);
    this.value = Object.values(card);
  }
  randomItem(items) {
    return items[Math.floor(Math.random() * items.length)];
  }

  randomCard() {
    let card = this.randomItem(deck);
    return card;
  }
}
