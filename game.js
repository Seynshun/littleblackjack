let player = new Player(50, 0);
let bank = new Player(2000, 0);
let bet = 0;
const maxPlayerValue = 21;
const maxBankValue = 17;
const minBet = 5;
const maxBet = 200;

document.querySelector("#myMoney").innerHTML = player.getMoney();
document.querySelector("#bankMoney").innerHTML = bank.getMoney();
document.querySelector("#gameLevel").innerHTML = player.getGameValue();
document.querySelector("#bankLevel").innerHTML = bank.getGameValue();

const btnNewCard = document.querySelector("#newCard");
const btnPassTurn = document.querySelector("#passTurn");
const btnStartGame = document.querySelector("#validateBet");
const btnRestart = document.querySelector("#restartGame");

btnNewCard.addEventListener("click", newCard);
btnPassTurn.addEventListener("click", passTurn);
btnStartGame.addEventListener("click", startGame);
btnRestart.addEventListener("click", restartGame);

function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

function enableButton() {
  btnNewCard.disabled = false;
  btnStartGame.disabled = true;
  btnPassTurn.className = "btn btn-danger";
  btnPassTurn.disabled = false;
}
function disableButton() {
  btnPassTurn.className = "btn btn-secondary";
  btnPassTurn.disabled = true;
  btnNewCard.disabled = true;
  btnStartGame.disabled = false;
}
async function startGame() {
  document.querySelector("#errorBet").innerHTML ="";
  bet = parseInt(document.getElementById("bet").value);

  if (bet >= minBet && bet <= maxBet) {
    newCard();
    await sleep(1000);
    newCard();
    await sleep(1000);
    enableButton();
  } else {
    document.querySelector("#errorBet").innerHTML =
      "La mise doit être entre 5 et 200$";
  }
}
async function winnerScreen() {
  player.updateMoney(bet);
  bank.updateMoney(-bet);
  document.querySelector("#myMoney").innerHTML = player.getMoney();
  document.querySelector("#bankMoney").innerHTML = bank.getMoney();
  document.querySelector("#hero").style.visibility = "hidden";
  document.querySelector("#gameOverScreen").style.visibility = "visible";
  document.querySelector("#gameOver").innerHTML = "YOU WIN !";
}

async function loserScreen() {
  player.updateMoney(-bet);
  bank.updateMoney(bet);
  document.querySelector("#myMoney").innerHTML = player.getMoney();
  document.querySelector("#bankMoney").innerHTML = bank.getMoney();
  document.querySelector("#hero").style.visibility = "hidden";
  document.querySelector("#gameOverScreen").style.visibility = "visible";
  document.querySelector("#gameOver").innerHTML = "GAME OVER";
}

async function newCard() {
  let card = new Card();
  player.updateGameValue(card.value);
  var img = document.createElement("img");
  img.src = card.path;
  document.querySelector("#myCard").appendChild(img);
  document.querySelector("#gameLevel").innerHTML = player.getGameValue();
  if (bank.getGameValue() < maxBankValue) {
    bankTurn();
  } else {
    verifyWinner();
  }
}

async function bankTurn() {
  await sleep(400);
  let card = new Card();
  bank.updateGameValue(card.value);
  var img = document.createElement("img");
  img.src = card.path;
  document.querySelector("#bankCards").appendChild(img);
  document.querySelector("#bankLevel").innerHTML = bank.getGameValue();
  verifyWinner();
}

async function verifyWinner() {
  if (
    bank.getGameValue() > maxPlayerValue &&
    player.getGameValue() <= maxPlayerValue
  ) {
    await sleep(1000);
    winnerScreen();
  } else if (player.getGameValue() > maxPlayerValue) {
    await sleep(1000);
    loserScreen();
  } else if (
    player.getGameValue() == maxPlayerValue &&
    bank.getGameValue() != maxPlayerValue
  ) {
    await sleep(1000);
    winnerScreen();
  }
}
async function passTurn() {
  if (bank.getGameValue() > maxBankValue - 1) {
    if (player.getGameValue() > bank.getGameValue()) {
      await sleep(1000);
      winnerScreen();
    } else {
      await sleep(1000);
      loserScreen();
    }
  } else {
    bankTurn();
  }
}

function restartGame() {
  document.querySelector("#hero").style.visibility = "visible";
  document.querySelector("#gameOverScreen").style.visibility = "hidden";
  document.querySelector("#gameOver").innerHTML = "";
  player.setGameValue(0);
  bank.setGameValue(0);
  bet = 0;
  var bankCard = document.querySelector("#bankCards");
  var src = document.querySelector("#myCard");
  bankCard.innerHTML = "";
  src.innerHTML = "";
  disableButton();
  document.querySelector("#gameLevel").innerHTML = player.getGameValue();
  document.querySelector("#bankLevel").innerHTML = bank.getGameValue();
}
